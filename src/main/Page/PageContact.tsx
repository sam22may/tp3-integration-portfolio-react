import React from "react";
import PublicIcon from '@mui/icons-material/Public';
import PhoneIphoneIcon from '@mui/icons-material/PhoneIphone';
import EmailIcon from '@mui/icons-material/Email';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import { getThemeValues } from "../components/ThemeColors"
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";


interface Props {
    theme: SupportedThemes
}

const PageContact = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <div className="d-flex align-items-center m-5">
                    <PhoneIphoneIcon style={{ color: themeValues.colors.title }} fontSize="large" />
                    <p className="fz-24 ml-2">418-473-4037</p>
                </div>
                <div className="d-flex align-items-center m-5">
                    <EmailIcon style={{ color: themeValues.colors.title }} fontSize="large" />
                    <p className="fz-24 ml-2">sam22may<AlternateEmailIcon fontSize="medium" />hotmail.com</p>
                </div>
                <div className="d-flex align-items-center m-5">
                    <PublicIcon style={{ color: themeValues.colors.title }} fontSize="large" />
                    <p className="fz-24 ml-2">Saint-Augustin-de-Desmaures, Québec, Canada</p>
                </div>
            </div>
        </main>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(PageContact)