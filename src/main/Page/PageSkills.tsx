import React from "react";
import BeenhereIcon from '@mui/icons-material/Beenhere';
import notes_s1 from '../../images/competences/notes_s1.png';
import notes_s2 from '../../images/competences/notes_s2.png';
import { getThemeValues } from "../components/ThemeColors"
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes
}

const PageSkills = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('Skills');
    return (
        <div style={{ backgroundColor: themeValues.colors.mainBg }} >
            <main style={{ color: themeValues.colors.text1 }} className="container p-4 min-vh">
                <h5 style={{ color: themeValues.colors.title }}>{t("level")}</h5>
                <p><BeenhereIcon className="color-red" /> {t("low")}</p>
                <p><BeenhereIcon className="color-yellow" /> {t("moderate")}</p>
                <p><BeenhereIcon className="color-blue" /> {t("good")}</p>
                <p><BeenhereIcon className="color-green" /> {t("very")}</p>
                <hr />
                <div className="row mt-4">
                    <div className="col-md-4 text-center mb-4">
                        <h4 style={{ color: themeValues.colors.title }}>{t("create")}</h4>
                        <BeenhereIcon className="color-green" />
                        <p>{t("project")}</p>
                    </div>
                    <div className="col-md-4 text-center mb-4">
                        <h4 style={{ color: themeValues.colors.title }}>{t("integration")}</h4>
                        <BeenhereIcon className="color-green" />
                        <p>HTML / CSS / Frameworks</p>
                    </div>
                    <div className="col-md-4 text-center mb-4">
                        <h4 style={{ color: themeValues.colors.title }}>Front-end</h4>
                        <BeenhereIcon className="color-green" />
                        <p>{t("visual")} / Responsive / Animations</p>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-md-4 text-center mb-4">
                        <h4 style={{ color: themeValues.colors.title }}>{t("conception")}</h4>
                        <BeenhereIcon className="color-blue" />
                        <p>Logos / Wireframes / {t("models")}</p>
                    </div>
                    <div className="col-md-4 text-center mb-4">
                        <h4 style={{ color: themeValues.colors.title }}>Programmation</h4>
                        <BeenhereIcon className="color-blue" />
                        <p>{t("features")}</p>
                    </div>
                    <div className="col-md-4 text-center mb-4">
                        <h4 style={{ color: themeValues.colors.title }}>Back-end</h4>
                        <BeenhereIcon className="color-blue" />
                        <p>{t("server")}</p>
                    </div>
                </div>
                <hr />
                <div className="row mt-4">
                    <div className="col-md-4">
                        <h4 style={{ color: themeValues.colors.title }} className="text-center">Applications/{t("software")}</h4>
                        <ul>
                            <li>Visual Code Studio</li>
                            <li>GIT</li>
                            <li>Communications :
                                <ul>
                                    <li>Teams</li>
                                    <li>Zoom</li>
                                    <li>Discord</li>
                                </ul>
                            </li>
                            <li>Design :
                                <ul>
                                    <li>Photoshop</li>
                                    <li>Illustrator</li>
                                    <li>Figma</li>
                                </ul>
                            </li>
                            <li>Postman</li>
                            <li>Adobe Audition</li>
                            <li>Adobe After Effect</li>
                            <li>Adobe Premiere Pro</li>
                            <li>Suite Office</li>
                        </ul>
                    </div>
                    <div className="col-md-4">
                        <h4 style={{ color: themeValues.colors.title }} className="text-center">Languages/Technologies</h4>
                        <div>
                            <ul className="pad-all-child">
                                <li className="bb1">
                                    <div className="d-flex justify-content-between">
                                        <p>HTML</p>
                                        <BeenhereIcon className="color-green" />
                                    </div>
                                </li>
                                <li>
                                    <div className="bb1 d-flex justify-content-between">
                                        <p>CSS/SCSS</p>
                                        <BeenhereIcon className="color-green" />
                                    </div>
                                    <ul className="pad-all-child">
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Bootstrap</p>
                                                <BeenhereIcon className="color-green" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Tailwind</p>
                                                <BeenhereIcon className="color-blue" />
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div className="bb1 d-flex justify-content-between">
                                        <p>Javascript</p>
                                        <BeenhereIcon className="color-blue" />
                                    </div>

                                    <ul className="pad-all-child">
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>JQuery</p>
                                                <BeenhereIcon className="color-green" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Express</p>
                                                <BeenhereIcon className="color-blue" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>React</p>
                                                <BeenhereIcon className="color-blue" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Vue</p>
                                                <BeenhereIcon className="color-yellow" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Typescript</p>
                                                <BeenhereIcon className="color-yellow" />
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div className="bb1 d-flex justify-content-between">
                                        <p>PHP</p>
                                        <BeenhereIcon className="color-blue" />
                                    </div>
                                    <ul className="pad-all-child">
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Laravel</p>
                                                <BeenhereIcon className="color-blue" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Wordpress</p>
                                                <BeenhereIcon className="color-yellow" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>Joomla</p>
                                                <BeenhereIcon className="color-yellow" />
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li className="bb1">
                                    <div className="d-flex justify-content-between">
                                        <p>Python</p>
                                        <BeenhereIcon className="color-red" />
                                    </div>
                                </li>
                                <li>
                                    <div className="bb1 d-flex justify-content-between">
                                        <p>{t("database")}</p>
                                        <BeenhereIcon className="color-blue" />
                                    </div>
                                    <ul className="pad-all-child">
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>MySQL</p>
                                                <BeenhereIcon className="color-blue" />
                                            </div>
                                        </li>
                                        <li className="bb1">
                                            <div className="d-flex justify-content-between">
                                                <p>MongoDB</p>
                                                <BeenhereIcon className="color-blue" />
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <h4 style={{ color: themeValues.colors.title }} className="text-center">{t("others")}</h4>
                        <ul>
                            <li>Shopify</li>
                            <li>Méthodologie Agile</li>
                            <li>Tableau Kanban</li>
                        </ul>
                    </div>
                </div>

                <hr />
                <div>
                    <h4 style={{ color: themeValues.colors.title }}>AEC en développement web 2020-2021 - cégep Garneau</h4>
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <h5>Session 1</h5>
                            <img src={notes_s1} alt="notes session 1" className="mw-100" />
                        </div>
                        <div className="col-md-6 col-sm-12">
                            <h5>Session 2</h5>
                            <img src={notes_s2} alt="notes session 2" className="mw-100" />
                        </div>
                    </div>
                    <div>

                    </div>

                </div>
            </main>
        </div>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(PageSkills)