import React from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import DescriptionIcon from '@mui/icons-material/Description';
import immo1 from "../../../images/projets/immologi/immologi_Page_1.jpg";
import immo2 from "../../../images/projets/immologi/immologi_Page_2.jpg";
import immo3 from "../../../images/projets/immologi/immologi_Page_3.jpg";
import immo4 from "../../../images/projets/immologi/immologi_Page_4.jpg";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetImmologi = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects/agenceimmologi" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/agenceimmologi/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp2-prog-logements" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Link to="/projects/agenceimmologi" className="btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Agence Immologi
                    </Link>
                </nav>

                <div className="mt-4">
                    <div className="">
                        <h2>Devis</h2>
                    </div>
                    <div className="">
                        <img src={immo1} alt="immo" className="mw-100" />
                        <img src={immo2} alt="immo" className="mw-100" />
                        <img src={immo3} alt="immo" className="mw-100" />
                        <img src={immo4} alt="immo" className="mw-100 mb-4" />
                    </div>
                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(ProjetImmologi)