import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import motocross1 from "../../../images/projets/motocross/Capture-accueil1.png";
import motocross2 from "../../../images/projets/motocross/Capture-accueil2.png";
import motocross3 from "../../../images/projets/motocross/Capture-creation.png";
import motocross4 from "../../../images/projets/motocross/Capture-galerie.png";
import motocross5 from "../../../images/projets/motocross/Capture-default.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const Projetmotocross = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/motocross/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp2-inte-bootstrap" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Button disabled>
                        Projet Motocross
                    </Button>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Motocross</h2>
                        <p>AEC développement web</p>
                        <p>Intégration 2 - Travail pratique 2</p>
                        <p>Bootstrap, Sass, Javascript</p>
                        <p>Note : 93%</p>
                        <hr />
                        <h4>Résumé</h4>
                        <p className="text-justify">Créer un site au sujet de notre choix avec l'aide de Bootstrap et de Sass. Doit contenir un carrousel sur la page d'accueil, une page création, galerie et un exemple (par défault).</p>
                    </div>
                    <div className="my-4">
                        <img src={motocross1} alt="motocross" className="mw-100 mb-4" />
                        <img src={motocross2} alt="motocross" className="mw-100 mb-4" />
                        <img src={motocross3} alt="motocross" className="mw-100 mb-4" />
                        <img src={motocross4} alt="motocross" className="mw-100 mb-4" />
                        <img src={motocross5} alt="motocross" className="mw-100 mb-4" />
                    </div>
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(Projetmotocross)