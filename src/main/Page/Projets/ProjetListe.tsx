import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import ListeLogo from '../../../images/projets/liste_logo.png';
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetListe = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/liste/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <Link to="/projects/liste/documentation" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1"> Documentation</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp4-liste-epicerie" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Button disabled style={{ color: themeValues.colors.btn }}>
                        Projet Listé
                    </Button>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Listé</h2>
                        <p>AEC développement web</p>
                        <p>Intégration 2</p>
                        <p>Travail Pratique 4 - Liste d'épicerie PWA</p>
                        <p>Note : 100%</p>
                        <h5>Technologies utilisés</h5>
                        <p>VUE.Js, Python, SMACSS</p>
                        <h5>Objectif</h5>
                        <p>Le but de ce travail est de créer une application avec vuejs qui rassemble plusieurs éléments que nous avons vus cette session.</p>
                        <h5>Description</h5>
                        <p>Vous devez créer une application PWA pour mobile  et pour grand écran permettant de créer une liste d’épicerie et d’en calculer le total.</p>
                        <p>Il y aura cinq fenêtres : </p>
                        <ul>
                            <li>Page d’accueil contenant les listes créées, le bouton créer liste et un lien vers la liste de tous les produits;</li>
                            <li>Formulaire pour créer une liste d’épicerie :
                                <ul>
                                    <li>Nom de la liste : requis;</li>
                                </ul>
                            </li>
                            <li>Les listes créées;</li>
                            <li>Une liste d’épicerie qui contient ou non des items :
                                <ul>
                                    <li>Il est possible de réduire ou augmenter la quantité d’un item;</li>
                                    <li>Le prix total est affiché;</li>
                                    <li>Le prix pour chaque produit est affiché;</li>
                                </ul>
                            </li>
                            <li>Une page qui contient tous les produits disponibles dans le système;</li>
                        </ul>

                    </div>
                    <div className="col-md-3 d-flex flex-wrap justify-content-center">
                        <div>
                            <a className="mb-3 btn-link text-center" href="https://www.figma.com/file/5GaNDQsA0YuP28GAqlmmgv/TP4---Liste-%C3%A9picerie?node-id=101%3A1567" target="_blank" rel="noreferrer">Maquette figma</a>
                            <div>
                                <img src={ListeLogo} alt="logo" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(ProjetListe)