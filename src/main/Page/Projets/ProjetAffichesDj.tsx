import React from "react";
import { Link } from "react-router-dom";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import affiche1 from "../../../images/projets/affiches/party_dance.jpg";
import affiche2 from "../../../images/projets/affiches/party_halloween.jpg";
import affiche3 from "../../../images/projets/affiches/party_latino.jpg";
import { getThemeValues } from "../../components/ThemeColors"
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const ProjetAffiches = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } =  useTranslation("ProjectsNav");
    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Button disabled style={{ color: themeValues.colors.btn }}>
                        Projet Affiches
                    </Button>
                </nav>

                <div className="mt-4">
                    <div className="">
                        <h2>Affiches publicitaire DJ</h2>
                    </div>
                    <div className="">
                        <h4>Party dance</h4>
                        <img src={affiche1} alt="affiche" className="mw-100" />
                        <h4>Party halloween</h4>
                        <img src={affiche2} alt="affiche" className="mw-100" />
                        <h4>Party latino</h4>
                        <img src={affiche3} alt="affiche" className="mw-100" />
                    </div>
                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(ProjetAffiches)