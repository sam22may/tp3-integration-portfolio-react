import React from "react";
import { Link } from "react-router-dom";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import trnx1 from "../../../images/projets/trnx/TRNXshop-screenshot-page-accueil.png";
import trnx2 from "../../../images/projets/trnx/TRNXshop-screenshot-page-cable.png";
import trnx3 from "../../../images/projets/trnx/TRNXshop-screenshot-page-earphones.png";
import trnx4 from "../../../images/projets/trnx/TRNXshop-screenshot-page-lighter.png";
import trnx5 from "../../../images/projets/trnx/TRNXshop-screenshot-page-shop.png";
import trnx6 from "../../../images/projets/trnx/TRNXshop-screenshot-page-support.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetTrnx = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } =  useTranslation("ProjectsNav");

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn}}>
                        <ArrowBackIcon /> 
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Button disabled style={{ color: themeValues.colors.btn}}>
                        Projet TRNXshop
                    </Button>
                </nav>

                <div className="mt-4">
                    <div className="">
                        <h2>TRNXshop.com</h2>
                        <p>Boutique en ligne (fermé) de ventes de produits électroniques (écouteurs sans-fil)</p>
                        <p>Réalisé avec Shopify</p>
                        <p>Projet personnel</p>
                    </div>
                    <div className="">
                        <h4>Page Accueil</h4>
                        <img src={trnx1} alt="trnx shop" className="mw-100" />
                        <h4>Produit : cables</h4>
                        <img src={trnx2} alt="trnx shop" className="mw-100" />
                        <h4>Produit : écouteur sans-fil</h4>
                        <img src={trnx3} alt="trnx shop" className="mw-100" />
                        <h4>Produit : Lighter électrique</h4>
                        <img src={trnx4} alt="trnx shop" className="mw-100" />
                        <h4>All products</h4>
                        <img src={trnx5} alt="trnx shop" className="mw-100" />
                        <h4>Page support</h4>
                        <img src={trnx6} alt="trnx shop" className="mw-100" />
                    </div>
                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetTrnx)