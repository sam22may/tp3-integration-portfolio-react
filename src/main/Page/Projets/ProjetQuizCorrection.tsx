import React from "react";
import { Link } from "react-router-dom";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import correction from "../../../images/projets/quiz/correction.png"
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetQuizCorrection = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } =  useTranslation("ProjectsNav");

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects/quiz" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/quiz/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <Link to="/projects/quiz/correction" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">Correction</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp4-jquery-quiz" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Link to="/projects/quiz" className="btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Quiz
                    </Link>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Correction</h2>


                    </div>
                    <div className="">
                        <img src={correction} alt="correction" className="mw-100" />
                    </div>

                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetQuizCorrection)