import React from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import { Link } from "react-router-dom";
import { getThemeValues } from "../../components/ThemeColors"
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { connect } from "react-redux";
import devisliste1 from "../../../images/projets/Liste_image/devis_liste_epicerie_Page_1.jpg";
import devisliste2 from "../../../images/projets/Liste_image/devis_liste_epicerie_Page_2.jpg";
import devisliste3 from "../../../images/projets/Liste_image/devis_liste_epicerie_Page_3.jpg";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetListe = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects/liste" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/liste/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <Link to="/projects/liste/documentation" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1"> Documentation</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp4-liste-epicerie" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Link to="/projects/liste" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Listé
                    </Link>
                </nav>
                <div className="my-4">
                    <h2>Devis</h2>
                    <img src={devisliste1} alt="devis 1" className="mw-100" />
                    <img src={devisliste2} alt="devis 2" className="mw-100" />
                    <img src={devisliste3} alt="devis 3" className="mw-100" />
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(ProjetListe)