import React from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import devis1 from "../../../images/projets/faitmonmenage/PP1_Page_01.jpg";
import devis2 from "../../../images/projets/faitmonmenage/PP1_Page_02.jpg";
import devis3 from "../../../images/projets/faitmonmenage/PP1_Page_03.jpg";
import devis4 from "../../../images/projets/faitmonmenage/PP1_Page_04.jpg";
import devis5 from "../../../images/projets/faitmonmenage/PP1_Page_05.jpg";
import devis6 from "../../../images/projets/faitmonmenage/PP1_Page_06.jpg";
import devis7 from "../../../images/projets/faitmonmenage/PP1_Page_07.jpg";
import devis8 from "../../../images/projets/faitmonmenage/PP1_Page_08.jpg";
import devis9 from "../../../images/projets/faitmonmenage/PP1_Page_09.jpg";
import devis10 from "../../../images/projets/faitmonmenage/PP1_Page_10.jpg";
import devis11 from "../../../images/projets/faitmonmenage/PP1_Page_11.jpg";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetFaitMonMenageDevis = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects/faitmonmenage" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/faitmonmenage/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/pp1-faitmonmenage" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Link to="/projects/faitmonmenage" className="btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Faitmonmenage.com
                    </Link>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Devis</h2>
                    </div>
                    <div className="">
                        <img src={devis1} alt="devis" className="mw-100" />
                        <img src={devis2} alt="devis" className="mw-100" />
                        <img src={devis3} alt="devis" className="mw-100" />
                        <img src={devis4} alt="devis" className="mw-100" />
                        <img src={devis5} alt="devis" className="mw-100" />
                        <img src={devis6} alt="devis" className="mw-100" />
                        <img src={devis7} alt="devis" className="mw-100" />
                        <img src={devis8} alt="devis" className="mw-100" />
                        <img src={devis9} alt="devis" className="mw-100" />
                        <img src={devis10} alt="devis" className="mw-100" />
                        <img src={devis11} alt="devis" className="mw-100 mb-4" />
                    </div>
                </div>
            </div>
        </main>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetFaitMonMenageDevis)