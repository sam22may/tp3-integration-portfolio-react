import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import LanguageIcon from '@mui/icons-material/Language';
import nourriture from "../../../images/projets/nourriture/Capture-accueil.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const ProjetFaitMonMenage = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/nourriture/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://nourriture-communautaire.slemay.devwebgarneau.com/" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <LanguageIcon />
                        <div className="ml-1">Site web</div>
                    </a>
                    <a href="https://gitlab.com/sam22may/tp3_prog_food" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1">GIT</div>
                    </a>
                    <Button disabled>
                        Projet Nourriture Communautaire
                    </Button>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Nourriture Communautaire</h2>
                        <p>AEC développement web</p>
                        <p>Programmation 2</p>
                        <p>PHP, Laravel, MySQL</p>
                        <p>Note : en cours de correction</p>
                        <hr />
                        <h4>Résumé</h4>
                        <p className="text-justify">Développer une application qui permet à l'utilisateur de consulter ou de publier sur la page de la nourriture à donnée.  L'utilisateur peux se créer et gérer un profil. Il doit être connecté pour ajouter ou réservé un don de nourriture.  On affiche la photo de la nourritue ou de l'endroit, depuis combien de temps la nourriture est disponible, la ville du créateur de la publication, la température est afficher selon la ville et elle provient d'un API, apelle AJAX, la description, le nom et la photo du donateur.</p>
                    </div>
                    <div className="mt-4">
                        <img src={nourriture} alt="motocross" className="mw-100 mb-4" />
                    </div>
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetFaitMonMenage)