import React from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import LanguageIcon from '@mui/icons-material/Language';
import devis1 from "../../../images/projets/nourriture/devis_Page_1.jpg";
import devis2 from "../../../images/projets/nourriture/devis_Page_2.jpg";
import devis3 from "../../../images/projets/nourriture/devis_Page_3.jpg";
import devis4 from "../../../images/projets/nourriture/devis_Page_4.jpg";
import devis5 from "../../../images/projets/nourriture/devis_Page_5.jpg";
import devis6 from "../../../images/projets/nourriture/devis_Page_6.jpg";
import devis7 from "../../../images/projets/nourriture/devis_Page_7.jpg";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const ProjetFaitMonMenage = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects/nourriture" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/nourriture/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://nourriture-communautaire.slemay.devwebgarneau.com/" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <LanguageIcon />
                        <div className="ml-1">Site web</div>
                    </a>
                    <a href="https://gitlab.com/sam22may/tp3_prog_food" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1">GIT</div>
                    </a>
                    <Link to="/projects/nourriture" className="btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Nourriture Communautaire
                    </Link>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Devis</h2>
                    </div>
                    <div className="">
                        <img src={devis1} alt="motocross" className="mw-100 mb-4" />
                        <img src={devis2} alt="motocross" className="mw-100 mb-4" />
                        <img src={devis3} alt="motocross" className="mw-100 mb-4" />
                        <img src={devis4} alt="motocross" className="mw-100 mb-4" />
                        <img src={devis5} alt="motocross" className="mw-100 mb-4" />
                        <img src={devis6} alt="motocross" className="mw-100 mb-4" />
                        <img src={devis7} alt="motocross" className="mw-100 mb-4" />
                    </div>
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetFaitMonMenage)