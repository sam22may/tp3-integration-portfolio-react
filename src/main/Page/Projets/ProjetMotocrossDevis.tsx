import React from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import motocross1 from "../../../images/projets/motocross/Tp2-bootstrap_Page_1.jpg";
import motocross2 from "../../../images/projets/motocross/Tp2-bootstrap_Page_2.jpg";
import motocross3 from "../../../images/projets/motocross/Tp2-bootstrap_Page_3.jpg";
import motocross4 from "../../../images/projets/motocross/Tp2-bootstrap_Page_4.jpg";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetmotocrossDevis = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                <Link to="/projects/motocross" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/motocross/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp2-inte-bootstrap" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Link to="/projects/motocross" className="btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Motocross
                    </Link>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Devis</h2>
                    </div>
                    <div className="">
                        <img src={motocross1} alt="motocross" className="mw-100" />
                        <img src={motocross2} alt="motocross" className="mw-100" />
                        <img src={motocross3} alt="motocross" className="mw-100" />
                        <img src={motocross4} alt="motocross" className="mw-100 mb-4" />
                    </div>
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetmotocrossDevis)