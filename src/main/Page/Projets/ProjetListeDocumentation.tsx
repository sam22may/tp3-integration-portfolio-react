import React, { useState } from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import ListeDocumentation1 from '../../../images/projets/Liste_image/Listé_Documentation_Page_01.jpg';
import ListeDocumentation2 from '../../../images/projets/Liste_image/Listé_Documentation_Page_02.jpg';
import ListeDocumentation3 from '../../../images/projets/Liste_image/Listé_Documentation_Page_03.jpg';
import ListeDocumentation4 from '../../../images/projets/Liste_image/Listé_Documentation_Page_04.jpg';
import ListeDocumentation5 from '../../../images/projets/Liste_image/Listé_Documentation_Page_05.jpg';
import ListeDocumentation6 from '../../../images/projets/Liste_image/Listé_Documentation_Page_06.jpg';
import ListeDocumentation7 from '../../../images/projets/Liste_image/Listé_Documentation_Page_07.jpg';
import ListeDocumentation8 from '../../../images/projets/Liste_image/Listé_Documentation_Page_08.jpg';
import ListeDocumentation9 from '../../../images/projets/Liste_image/Listé_Documentation_Page_09.jpg';
import ListeDocumentation10 from '../../../images/projets/Liste_image/Listé_Documentation_Page_10.jpg';
import ListeDocumentation11 from '../../../images/projets/Liste_image/Listé_Documentation_Page_11.jpg';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { Link } from "react-router-dom";
import { getThemeValues } from "../../components/ThemeColors"
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const ProjetListe = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    const [image, setImage] = useState(ListeDocumentation1);

    const handleChange = (e: any, p: number) => {
        const page1 = ListeDocumentation1
        const page2 = ListeDocumentation2
        const page3 = ListeDocumentation3
        const page4 = ListeDocumentation4
        const page5 = ListeDocumentation5
        const page6 = ListeDocumentation6
        const page7 = ListeDocumentation7
        const page8 = ListeDocumentation8
        const page9 = ListeDocumentation9
        const page10 = ListeDocumentation10
        const page11 = ListeDocumentation11
        const allPages = [page1, page2, page3, page4, page5, page6, page7, page8, page9, page10, page11]

        let page = allPages[p - 1]

        setImage(page)
    }

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects/liste" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/liste/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <Link to="/projects/liste/documentation" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1"> Documentation</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp4-liste-epicerie" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Link to="/projects/liste" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        Projet Listé
                    </Link>
                </nav>
                <div className="my-4">
                    <Stack spacing={2}>
                        <Pagination onChange={handleChange} count={11} color="primary" />
                    </Stack>
                    <img src={image} alt="documentation" className="mw-100 mt-4" />
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetListe)