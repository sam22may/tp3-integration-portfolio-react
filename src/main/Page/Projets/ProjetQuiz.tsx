import React from "react";
import { Link } from "react-router-dom";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import quiz1 from "../../../images/projets/quiz/Capture.png";
import quiz2 from "../../../images/projets/quiz/Capture2.png";
import quiz3 from "../../../images/projets/quiz/Capture3.png";
import quiz4 from "../../../images/projets/quiz/Capture4.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetQuiz = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/quiz/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <Link to="/projects/quiz/correction" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">Correction</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp4-jquery-quiz" target="_blank" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }} rel="noreferrer">
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Button disabled style={{ color: themeValues.colors.btn }}>
                        Projet Quiz
                    </Button>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Quiz</h2>
                        <p>AEC développement web</p>
                        <p>Intégration 2 - Travail pratique 3</p>
                        <p>JQuery, JQueryUI, JQuery Validate</p>
                        <p>Note : 110%</p>
                        <hr />
                        <h4>Résumé</h4>
                        <p className="text-justify">Créer un formulaire d'inscription qui est validé à l'aide de JQuery Validate. Lorsque les données sont valides, le formulaire se transforme en questionnaire. À la fin, une page donne les résultats avec les informations du candidat provenant du formulaire.</p>
                    </div>
                    <div className="mt-4">
                        <img src={quiz1} alt="quiz" className="mw-100 mb-4" />
                        <img src={quiz2} alt="quiz" className="mw-100 mb-4" />
                        <img src={quiz3} alt="quiz" className="mw-100 mb-4" />
                        <img src={quiz4} alt="quiz" className="mw-100 mb-4" />
                    </div>
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetQuiz)