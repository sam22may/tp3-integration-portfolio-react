import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import LanguageIcon from '@mui/icons-material/Language';
import portfolioLogo from "../../../images/projets/Portfolio/capture-accueil2.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetPremierPortfolio = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/premierportfolio/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="http://monpremierportfolio.s3-website.ca-central-1.amazonaws.com/" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <LanguageIcon />
                        <div className="ml-1">Site web</div>
                    </a>
                    <a href="https://gitlab.com/sam22may/tp4-portfolio" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Button disabled style={{ color: themeValues.colors.btn }}>
                        Projet Premier Portfolio
                    </Button>
                </nav>

                <div className="mt-4">
                    <div className="">
                        <h2>Premier portfolio</h2>
                        <p>AEC développement web</p>
                        <p>Intégration 1 - Travail pratique 4</p>
                        <p>Note : 100%</p>
                    </div>
                    <div className="">
                        <img src={portfolioLogo} alt="logo" className="mw-100" />
                    </div>
                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(ProjetPremierPortfolio)