import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import DescriptionIcon from '@mui/icons-material/Description';
import immoaccueil from "../../../images/projets/immologi/Capture-immo-accueil.png";
import immoajout from "../../../images/projets/immologi/Capture-ajout.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes,
}

const ProjetImmologi = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/agenceimmologi/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/tp2-prog-logements" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Button disabled style={{ color: themeValues.colors.btn }}>
                        Projet Agence Immologi
                    </Button>
                </nav>

                <div className="mt-4">
                    <div className="col-md-6">
                        <h2>Agence Immologi</h2>
                        <p>AEC développement web</p>
                        <p>Programmation 2 - Travail pratique 2 site web agence immobilière</p>
                        <p>PHP, MySQL</p>
                        <p>Note : 100%</p>
                        <hr />
                        <h4>Résumé</h4>
                        <p className="text-justify">Réaliser le prototype d'un site Internet pour une agence immobilière de location et de vente de logements.  Afficher tout les logements ou seulement les logements en location ou à vendre provenant d'une base de donnée.  Ajouter, modifier, supprimer les logements.</p>
                    </div>
                    <div className="mt-4">
                        <img src={immoaccueil} alt="accueil" className="mw-100 mb-4" />
                        <img src={immoajout} alt="ajout" className="mw-100 mb-4" />
                    </div>
                </div>

            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(ProjetImmologi)