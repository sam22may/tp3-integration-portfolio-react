const fetchCats = async(page) => {
    const limit = 12
    const url = `https://api.thecatapi.com/v1/images/search?limit=${limit}&page=${page}&order=desc`
    const response = await fetch(url, {
        method: "GET",
        headers: {
            "x-api-key": "5f79fb88-7c39-4434-99ba-224ec9945ae8"
        }
    })

    const totalImage = response.headers.get("pagination-count")
    const totalPagesApi = Math.ceil(totalImage / limit - 1)

    const data = await response.json()

    const cats = data.map((cat) => {
        return {
            id: cat.id,
            url: cat.url,
        }
    })

    return {
        cats,
        totalPagesApi,
    }

}

export { fetchCats }