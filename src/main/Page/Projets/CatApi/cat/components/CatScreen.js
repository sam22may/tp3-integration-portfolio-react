import React from "react";
import { fetchCats } from "../api/CatApi";
import PropTypes from 'prop-types';
import { fetchRandomCat } from "../api/RandomCat";

const propTypes = {
    items: PropTypes.array.isRequired,
    onChangePage: PropTypes.func.isRequired,
    initialPage: PropTypes.number,
    pageSize: PropTypes.number
}

const defaultProps = {
    initialPage: 1,
    pageSize: 10
}

class CatScreen extends React.Component {
        state = { 
            pager: {},
            cats: [],
            pageCount: 2,
        };

    componentDidMount() {
        // set page if items array isn't empty
            this.setPage(this.props.initialPage);
    }

    setPage = async (page) => {
        var { items, pageSize } = this.props;
        var pager = this.state.pager;
        await this.getCats(page)


        if (page < 1 || page > pager.totalPages) {

            return;
        }

        // get new pager object for specified page
        pager = this.getPager(items.length, page, pageSize);

        // get new page of items from items array
        var pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

        // update state
        this.setState({ pager: pager });

        // call change page function in parent component
        this.props.onChangePage(pageOfItems);
    }

    getCats = async (page) => {
        const getCats = fetchCats(page)
        const result = await getCats;
        this.setState({ cats: result.cats, pageCount: result.totalPagesApi })
    }

    setRandomCat  = async () => {
        const randomCat = fetchRandomCat()
        this.setState({ cats: await randomCat })
    }

    getPager = (totalItems, currentPage, pageSize) =>{
        // default to first page
        currentPage = currentPage || 1;

        // default page size is 10
        pageSize = pageSize || 10;

        // calculate total pages
        var totalPages = this.state.pageCount

        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // create an array of pages to ng-repeat in the pager control
        var pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            pages: pages
        };
    }

    render() {
        var pager = this.state.pager;

        if (!pager.pages || pager.pages.length <= 1) {
            // don't display pager if there is only 1 page
            return null;
        }

        return (
            <>
            <h1 className="h1-cat">Le réseau de domination mondial des chats</h1>
            <div className="container d-flex flex-wrap">
                {this.state.cats.map((cat) => (
                    <div className="country-card d-flex">
                      <div className="country-card-container border-gray rounded border mx-2 my-3 d-flex flex-row align-items-center p-0 bg-light shadow">
                        <div className="img-max-w position-relative border-gray border-right px-2 bg-white rounded-left">
                          <img className="w-100" key={cat.id} src={cat.url} alt="chat"/>
                        </div>
                        <div className="px-3">
                          <span className="country-name text-dark d-block font-weight-bold">Nom de code:</span>
                          <a href={cat.url} className="country-region text-secondary text-uppercase">{cat.id}</a>
                        </div>
                      </div>
                    </div>
                ))}
            </div>
            <div className="d-flex justify-content-between">
                <ul className="pagination">
                    <li className={pager.currentPage === 1 ? 'page-text disabled' : 'page-text'}>
                        <div onClick={() => this.setPage(1)}>1...</div>
                    </li>
                    <li className={pager.currentPage === 1 ? 'page-text disabled' : 'page-text'}>
                        <div onClick={() => this.setPage(pager.currentPage - 1)}>Précédent</div>
                    </li>
                    {pager.pages.map((page, index) =>
                        <li key={index} className={pager.currentPage === page ? 'page-number active' : 'page-number'}>
                            <div onClick={() => this.setPage(page)}>{page}</div>
                        </li>
                    )}
                    <li className={pager.currentPage === pager.totalPages ? 'page-text disabled' : 'page-text'}>
                        <div onClick={() => this.setPage(pager.currentPage + 1)}>Suivant</div>
                    </li>
                    <li className={pager.currentPage === pager.totalPages ? 'page-text disabled' : 'page-text'}>
                        <div onClick={() => this.setPage(pager.totalPages)}>...{pager.totalPages}</div>
                    </li>
                </ul>
                <div>
                    <div className="btn-rdm" onClick={() => this.setRandomCat()}>Agent Aléatoire</div>
                </div>
            </div>
            </>
        );
    }
}

CatScreen.propTypes = propTypes;
CatScreen.defaultProps = defaultProps;
export default CatScreen;