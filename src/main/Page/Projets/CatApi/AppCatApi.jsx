import React from 'react';
import CatScreen from './cat/components/CatScreen';

class CatApi extends React.Component {
    constructor() {
        super();

        // an example array of 150 items to be paged
        var exampleItems = [...Array(150).keys()].map(i => ({ id: (i+1), name: 'Item ' + (i+1) }));

        this.state = {
            exampleItems: exampleItems,
            pageOfItems: []
        };

        // bind function in constructor instead of render (https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md)
        this.onChangePage = this.onChangePage.bind(this);
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }
    
    render() {
        return (
            <div> 
                <div className="container2">
                    <div className="text-center">
                        <CatScreen items={this.state.exampleItems} onChangePage={this.onChangePage} />
                    </div>
                </div>
                <hr />
                <div className="credits text-center">
                    <p>TP2 - intégration 3 - The Cat Api</p>
                    <p>Samuel Le May</p>
                </div>
            </div>
        );
    }
}

export default CatApi;