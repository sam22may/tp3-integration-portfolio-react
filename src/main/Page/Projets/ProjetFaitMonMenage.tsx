import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DescriptionIcon from '@mui/icons-material/Description';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import faitmonmenage from "../../../images/projets/faitmonmenage/Capture-PP1.png";
import faitmonmenage1 from "../../../images/projets/faitmonmenage/Capture-accueil.png";
import faitmonmenage2 from "../../../images/projets/faitmonmenage/Capture-accueil1.png";
import faitmonmenage3 from "../../../images/projets/faitmonmenage/Capture-equipe.png";
import faitmonmenage4 from "../../../images/projets/faitmonmenage/Capture-connexion.png";
import faitmonmenage5 from "../../../images/projets/faitmonmenage/Capture-inscription.png";
import faitmonmenage6 from "../../../images/projets/faitmonmenage/Capture-profil.png";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const ProjetFaitMonMenage = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/faitmonmenage/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <a href="https://gitlab.com/sam22may/pp1-faitmonmenage" target="_blank" rel="noreferrer" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <AccountTreeIcon />
                        <div className="ml-1"> GIT</div>
                    </a>
                    <Button disabled>
                        Projet Faitmonmenage.com
                    </Button>
                </nav>

                <div className="row mt-4">
                    <div className="col-md-6">
                        <h2>Faitmonmenage.com</h2>
                        <p>AEC développement web</p>
                        <p>Projet de production 1</p>
                        <p>Excpress.Js, API, MongoDB</p>
                        <p>Note : 93%</p>
                        <hr />
                        <h4>Résumé</h4>
                        <p className="text-justify">Développer un site complet pour l'entreprise fictive faitmonmenage. Page d'accueil, à propos, blog et connexion. Avec une connexion différente pour utilisateur et administrateur. L'utilisateur pouvent gèrer son profil et engager des responsables de ménages. L'administrateur accédait à un CMS permettant de modifier les informations du site. Également pour publier, modifier, supprimer des articles du blog. Un API devait être également conçu pour la gestion des profils et des employés.</p>
                    </div>
                    <div className="col-md-6">
                        <img src={faitmonmenage} alt="menage" className="mw-100 my-4" />
                    </div>
                    <img src={faitmonmenage1} alt="menage" className="mw-100 my-4" />
                    <img src={faitmonmenage2} alt="menage" className="mw-100 mb-4" />
                    <img src={faitmonmenage3} alt="menage" className="mw-100 mb-4" />
                    <img src={faitmonmenage4} alt="menage" className="mw-100 mb-4" />
                    <img src={faitmonmenage5} alt="menage" className="mw-100 mb-4" />
                    <img src={faitmonmenage6} alt="menage" className="mw-100 mb-4" />
                </div>
            </div>
        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetFaitMonMenage)