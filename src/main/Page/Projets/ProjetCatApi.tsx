import React from "react";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import AppCatApi from "./CatApi/AppCatApi";
import DescriptionIcon from '@mui/icons-material/Description';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const ProjetCatApi = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('ProjectsNav');

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                    <Link to="/projects/catapi/devis" className="mr-3 btn-link" style={{ color: themeValues.colors.btn}}>
                        <DescriptionIcon />
                        <div className="ml-1">{t('work')}</div>
                    </Link>
                    <Button disabled style={{ color: themeValues.colors.btn }}>
                        Projet The Cat Api
                    </Button>
                </nav>

                <div className="mt-4">
                    <div className="">
                        <h2>The Cat Api</h2>
                        <p>AEC développement web</p>
                        <p>Intégration 3 - Travail pratique 2</p>
                        <p>React.js</p>
                        <p>Note : 100%</p>
                        <hr />
                        <h4>Résumé</h4>
                        <p>Créer une application React avec pagination qui va chercher des photos de chat avec un API.</p>
                    </div>
                    <div className="border mt-4">
                        <AppCatApi />
                    </div>
                </div>
            </div>
        </main>
    )
}


const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(ProjetCatApi)