import React from "react";
import { Link } from "react-router-dom";
import { getThemeValues } from "../components/ThemeColors"
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";


interface Props {
    theme: SupportedThemes
}

const PageCV = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="container p-4">
                <h2 style={{ color: themeValues.colors.title }} className="text-center">C.V.</h2>
                <h4 className="text-center">Samuel Le May</h4>
                <p className="text-center">Adresse sur demande</p>
                <p className="text-center">Saint-Augustin-de-Desmaures (Québec)</p>
                <p className="text-center">(418) 473-4037 / sam22may@hotmail.com</p>
                <h4 style={{ color: themeValues.colors.title }}>ATOUTS PROFESSIONNELS</h4>
                <ul>
                    <li>Bilingue</li>
                    <li>Facilité avec l'informatique</li>
                    <li>Créatif</li>
                    <li>Autonome</li>
                    <li>Apprends rapidement</li>
                    <li>Travail en équipe</li>
                    <li>Excellente condition physique</li>
                </ul>
                <h4 style={{ color: themeValues.colors.title }}>FORMATION SCOLAIRE</h4>
                <ul>
                    <li className="font-weight-bold">AEC en développement web au cégep Garneau, 2020-2021, en cours.</li>
                    <li>Diplôme d’études professionnelles en extraction du minerai, Centre Formation Professionnelle de la Baie-James à Chibougamau et Goldcorp Éléonore, complété juillet 2019. </li>
                    <li>Certificat en réalisation audionumérique; prise de son; cours complété à l’automne 2012, Université Laval.</li>
                    <li>Mathématiques de niveau collégial à l’automne 2011, Université Laval.</li>
                    <li className="font-weight-bold">Diplôme d’études collégiales, complété en 2011, Collège Mérici, programme Arts et lettres, profil Création Multimédia.</li>
                    <li>Diplôme d’études secondaires, complété en 2007, école secondaire Les Compagnons-de-Cartier, programme PROTIC.</li>
                </ul>
                <h4 style={{ color: themeValues.colors.title }}>EXPÉRIENCES DE TRAVAIL PERTINENTES</h4>
                <ul>
                    <li>Mai 2021 à aujourd’hui -  Tutorat pour le cégep Garneau. <a href="#references">Références</a>
                        <ul>
                            <li>Aider les élèves en difficulté.</li>
                        </ul>
                    </li>
                    <li>Février 2020 à août 2020 – Gestion d’une boutique en ligne TRNXshop.com (boutique fermée, voir <Link to="/projects/trnxshop">Projets</Link>)
                        <ul>
                            <li>Création de l’entreprise TRNX, boutique en ligne de produits électroniques.  Utilisation de Shopify pour la boutique.</li>
                            <li>Création de la marque, des designs, publicités facebook.  Gérer les commandes et livraisons.  Service à la clientèle.</li>
                        </ul>
                    </li>
                    <li>Février 2013 à janvier 2018 - Travailleur autonome comme DJ et promoteur d'événements. Voir <Link to="/projects/affichesdj">Projets</Link>.
                        <ul>
                            <li>Animation et DJ dans des événements. Montage/démontage de scène, création d’affiche publicitaire/publicité Facebook, gestion des contrats.</li>
                        </ul>
                    </li>
                    <li>Mai 2010 à septembre 2010 – QA / testeur jeux vidéo chez Beenox Québec.
                        <ul>
                            <li>Contrôle de qualité, détecter les bogues et anomalies dans les jeux.  Rédiger un rapport en anglais.  Reproduire les bogues.</li>
                        </ul>
                    </li>
                </ul>
                <h4 style={{ color: themeValues.colors.title }}>EXPÉRIENCES DE TRAVAIL (AUTRES)</h4>
                <ul>
                    <li>Mars 2012 à aujourd’hui - Magasinier d'entrepôt pour la quincaillerie Canac à St-Augustin-de-Desmaures.
                        <ul>
                            <li>Préparation / réception de commandes, opérateur de chariots élévateur.</li>
                        </ul>
                    </li>
                    <li>Octobre 2009 à mai 2010 - Février 2011 à février 2012 - Pompiste au garage Ultramar Éric Nadeau à St-Augustin-de-Desmaures.</li>
                    <li>Mai 2008 à Septembre 2009 - Cuisinier au restaurant La Clef des Champs à St-Augustin-de-Desmaures.</li>
                    <li>Février 2007 à mars 2008 - Soldat dans les Forces Armées Canadiennes aux Voltigeurs de Québec.</li>
                    <li>Été 2003 à 2007 - Différents emplois saisonniers.</li>
                </ul>
                <h4 style={{ color: themeValues.colors.title }}>ACTIVITÉS SOCIALES ET PRINCIPAUX INTÉRÊTS</h4>
                <p>Actif dans le mouvement scout pendant plus de 6 ans, j’ai appris jeune à développer ma débrouillardise, mon autonomie et à s’entraider en communauté.</p>
                <p>Engagé dans les Forces Armées Canadiennes, j’ai acquis plusieurs connaissances dont travailler en équipe dans des conditions difficiles.  Discipline et longues semaines de travail ne me font pas peur.</p>
                <p>Sportif, j’ai fait partie d’une équipe de soccer pendant plus de 8 ans.  Je continue à pratiquer différents sports pour le plaisir et m’entraîne régulièrement à la salle d’entraînement.</p>
                <p>Fasciné par toutes les nouvelles technologies, cela m'a apporté plusieurs passions. Je suis un grand amateur de jeux vidéo et de musique électronique.  J'ai déjà possédé mon studio de musique.</p>
                <br />
                <h4 style={{ color: themeValues.colors.title }} id="references">Références :</h4>
                <p>Tutorat cégep Garneau : Lyne Bélanger, 418-688-8310 poste 2380</p>
                <p>Canac : Jean-Pierre Hébert, 418-878-0080</p>
                <p>Travaux réalisés voir <Link to="/projects">Projets</Link></p>
                <p>Autres références sur demande</p>
            </div>
        </main>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(PageCV)