import React from "react";
import { getThemeValues } from "../components/ThemeColors"
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import SourceIcon from '@mui/icons-material/Source';
import InfoIcon from '@mui/icons-material/Info';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import { Link } from "react-router-dom";
import { ReactComponent as SLogo } from "../../images/s-devweb.svg";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes
}

const PageAccueil = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation("Home");

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <div className="text-center">
                    <SLogo className="w-50" style={{ fill: themeValues.colors.text1 }} />
                </div>
                <hr />
                <h2 style={{ color: themeValues.colors.title }} className="text-center">{t("title")}</h2>
                <div className="my-3 text-justify">
                    <p>{t("text")}</p>
                </div>
                <div className="row mt-4">
                    <div className="col-md-5"></div>
                    <div className="col-md-4">

                        <Link to="/presentation" className="pb-4 link-anim">
                            <div className="d-flex align-items-center ">
                                <AccessibilityIcon />
                                <h4 className="m-0 ml-2">{t("presentation")}</h4>
                            </div>
                        </Link>
                        <br />
                        <Link to="/skills" className="pb-4 link-anim">
                            <div className="d-flex align-items-center">
                                <AssignmentTurnedInIcon />
                                <h4 className="m-0 ml-2">{t("skills")}</h4>
                            </div>
                        </Link>
                        <br />
                        <Link to="/projects" className="pb-4 link-anim">
                            <div className="d-flex align-items-center">
                                <SourceIcon />
                                <h4 className="m-0 ml-2">{t("projects")}</h4>
                            </div>
                        </Link>
                        <br />
                        <Link to="/contact" className="pb-4 link-anim">
                            <div className="d-flex align-items-center">
                                <InfoIcon />
                                <h4 className="m-0 ml-2">Contact</h4>
                            </div>
                        </Link>
                        <br />
                        <Link to="/cv" className="pb-4 link-anim">
                            <div className="d-flex align-items-center">
                                <ContactPageIcon />
                                <h4 className="m-0 ml-2">{t("resume")}</h4>
                            </div>
                        </Link>
                    </div>
                    <div className="col-md-3"></div>
                </div>
                <hr />
            </div>
        </main>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(PageAccueil)