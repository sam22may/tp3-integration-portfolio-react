import React, { useState } from "react";
import { getThemeValues } from "../components/ThemeColors"
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActions } from '@mui/material';
import listeLogo from '../../images/projets/logo.svg';
import SendIcon from '@mui/icons-material/Send';
import LanguageIcon from '@mui/icons-material/Language';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import portfolioLogo from "../../images/projets/Portfolio/capture-accueil.png";
import affiche from "../../images/projets/affiches/Capture-party.png";
import immologi from "../../images/projets/immologi/Capture-immo.png";
import trnx from "../../images/projets/trnx/trnx.png";
import catApi from "../../images/projets/cat_api/Capture-catapi.png";
import quiz from "../../images/projets/quiz/Capture.png";
import motocross from "../../images/projets/motocross/Capture.png";
import faitmonmenage from "../../images/projets/faitmonmenage/Capture-PP1.png";
import nourriture from "../../images/projets/nourriture/Capture-accueil.png";
import warehouse from "../../images/projets/warehouse/Capture.png";
import tradingapp from "../../images/projets/tradingapp/Capture.png";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes
}

const PageProjets = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('Projects');
    const none = "none"
    const block = "block"
    const [perso, setPerso] = useState(block)
    const [aec, setAec] = useState(block)

    const filter = (e: any) => {
        let value = e.target.value
        if (value === "0") {
            setPerso(block)
            setAec(block)
        }
        if (value === "1") {
            setPerso(none)
            setAec(block)
        }
        if (value === "2") {
            setPerso(block)
            setAec(none)
        }
    }

    return (
        <main style={{ backgroundColor: themeValues.colors.mainBg }}>
            <div className="container min-vh py-5">
                <div className="d-flex mb-5">
                    <button value="0" onClick={filter} className="btn-link mr-4">Voir tous</button>
                    <button value="1" onClick={filter} className="btn-link mr-4">AEC développement web</button>
                    <button value="2" onClick={filter} className="btn-link">Projet personnel</button>
                </div>
                <div className="row row-cols-1 row-cols-xl-3 row-cols-md-2">
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="160"
                                    image={nourriture}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        {t('food')}
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        PHP, Laravel, MySQL
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('food_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Programmation 2
                                        <br />
                                        {t('grade')} en cours de correction
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/nourriture" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp3_prog_food" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="170"
                                    image={catApi}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        The Cat API
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        React.Js
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('cat_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Intégration 3
                                        <br />
                                        {t('grade')} 100%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/catapi" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp2-cat-api" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="160"
                                    image={immologi}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        Agence Immologi
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        PHP, MySQL
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('immologi_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Programmation 2
                                        <br />
                                        {t('grade')} 100%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/agenceimmologi" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp2-prog-logements" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="180"
                                    image={faitmonmenage}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        Faitmonmenage.com
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        Express.js, MongoDB
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('menage_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        {t('production_project')}
                                        <br />
                                        {t('grade')} 93%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/faitmonmenage" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/pp1-faitmonmenage" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg }}>
                            <div className="d-flex flex-column" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                                <CardMedia
                                    component="img"
                                    height="140"
                                    image={listeLogo}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        {t('list')}
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        VUE.Js, Python
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('list_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Intégration 2
                                        <br />
                                        {t('grade')} 100%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg }}>
                                <Link to="/projects/liste" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp4-liste-epicerie" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="180"
                                    image={quiz}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        Quiz
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        JQuery, JQueryUI
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('quiz_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Intégration 2
                                        <br />
                                        {t('grade')} 110%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/quiz" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp4-jquery-quiz" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="170"
                                    image={motocross}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        Motocross
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        Bootstrap, Javascript
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('moto_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Intégration 2
                                        <br />
                                        {t('grade')} 93%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/motocross" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp2-inte-bootstrap" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: aec }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between">
                            <div className="d-flex flex-column" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                                <CardMedia
                                    component="img"
                                    height="160"
                                    image={portfolioLogo}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        {t('first_portfolio')}
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        Bootstrap, Javascript
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('portfolio_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        Intégration 1
                                        <br />
                                        {t('grade')} 100%
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg }}>
                                <Link to="/projects/premierportfolio" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <a href="https://gitlab.com/sam22may/tp4-portfolio" target="_blank" rel="noreferrer" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-1">GIT</div>
                                    <AccountTreeIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: perso }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="170"
                                    image={tradingapp}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        Trading App
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        PHP
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        Application permettant de calculer la possiblité de gains en argent en tennant compte de la mise de départ, du prix de l'action et de la cible de vente.
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        {t('personnal')}
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Button disabled>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Button>
                                <a className="btn-link" href="https://tradingapp.slemay.devwebgarneau.com/" target="_blank" rel="noreferrer">
                                    <p className="mr-1">Lien</p>
                                    <LanguageIcon />
                                </a>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: perso }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="170"
                                    image={warehouse}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        Warehouse Management Simulator
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        Javascript
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        Warehouse Management Simulator
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        {t('personnal')}
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/warehouse" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <Button disabled size="small" color="primary" endIcon={<AccountTreeIcon />} >
                                    GIT
                                </Button>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: perso }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg, color: themeValues.colors.text1 }}>
                            <div className="d-flex flex-column">
                                <CardMedia
                                    component="img"
                                    height="95"
                                    image={trnx}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        TRNXshop.com
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        Shopify
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('trnx_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        {t('personnal')}
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/trnxshop" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <Button disabled size="small" color="primary" endIcon={<AccountTreeIcon />} >
                                    GIT
                                </Button>
                            </CardActions>
                        </Card>
                    </div>
                    <div style={{ display: perso }} className="col mb-5">
                        <Card sx={{ maxWidth: 345, minHeight: 410 }} className="d-flex flex-column justify-content-between" style={{ backgroundColor: themeValues.colors.cardBg }}>
                            <div className="d-flex flex-column" style={{ color: themeValues.colors.text1 }}>
                                <CardMedia
                                    component="img"
                                    height="150"
                                    image={affiche}
                                    alt="logo liste"
                                />
                                <CardContent>
                                    <Typography className="typo" gutterBottom variant="h5" component="div">
                                        {t('posters')}
                                    </Typography>
                                    <Typography className="typo" variant="body1">
                                        Photoshop
                                    </Typography>
                                    <Typography className="typo py-2" variant="body2">
                                        {t('posters_desc')}
                                    </Typography>
                                    <Typography className="typo" variant="body2">
                                        {t('personnal')}
                                    </Typography>
                                </CardContent>
                            </div>
                            <CardActions className="d-flex justify-content-between">
                                <Link to="/projects/affichesdj" className="btn-link" style={{ color: themeValues.colors.btn }}>
                                    <div className="mr-2">{t('learn')}</div>
                                    <SendIcon />
                                </Link>
                                <Button disabled size="small" color="primary" endIcon={<AccountTreeIcon />}>
                                    GIT
                                </Button>
                            </CardActions>
                        </Card>
                    </div>
                </div>
            </div>
        </main>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(PageProjets)