import React from "react";
import { Link } from "react-router-dom";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { useTranslation } from "react-i18next";
import "../warehouseSimulator/styles/css/style.css"

interface Props {
    theme: SupportedThemes,
}

const WarehouseGame = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation("ProjectsNav");

    return (
        <main style={{ backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                </nav>

                <div className="mt-4">
                    <div className="" style={{ color: themeValues.colors.text1 }}>
                        <h2>Warehouse Management Simulator</h2>
                        <p>En développement...</p>
                    </div>
                </div>
                <div className="bg">
                    <header className="mt-2">
                        <div className="d-flex">
                            <h1>Warehouse Management simulator</h1>
                            <button className="ml-4 button" id="startBtn">Get Lines</button>
                            <button className="ml-4 button" onclick="window.location.href='http://127.0.0.1:5500/index.html';">Menu</button>
                        </div>
                        <h2>Total lines : <span id="totallines"></span></h2>
                    </header>

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col">
                                <div className="row">
                                    <div className="col">
                                        <h2>Stores</h2>
                                        <h4 className="margb10">Mag 01</h4>
                                        <h4 className="margb10">Mag 02</h4>
                                        <h4 className="margb10">Mag 03</h4>
                                        <h4 className="margb10">Mag 04</h4>
                                        <h4 className="margb10">Mag 05</h4>
                                        <h4 className="margb10">Mag 06</h4>
                                        <h4 className="margb10">Mag 07</h4>
                                        <h4 className="margb10">Mag 08</h4>
                                        <h4 className="margb10">Mag 09</h4>
                                        <h4 className="margb10">Mag 10</h4>
                                    </div>
                                    <div className="col">
                                        <h2>Lines</h2>
                                        <h4 className="margb10" id="linesmag1"></h4>
                                        <h4 className="margb10" id="linesmag2"></h4>
                                        <h4 className="margb10" id="linesmag3"></h4>
                                        <h4 className="margb10" id="linesmag4"></h4>
                                        <h4 className="margb10" id="linesmag5"></h4>
                                        <h4 className="margb10" id="linesmag6"></h4>
                                        <h4 className="margb10" id="linesmag7"></h4>
                                        <h4 className="margb10" id="linesmag8"></h4>
                                        <h4 className="margb10" id="linesmag9"></h4>
                                        <h4 className="margb10" id="linesmag10"></h4>
                                    </div>
                                    <div className="col d-block">
                                        <h2>Worker</h2>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-1">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-2">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-3">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-4">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-5">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-6">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-7">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-8">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-9">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                        <div className="margb8">
                                            <select disabled className="select" name="worker-select-1" id="worker-select-10">
                                                <option value="">- Select -</option>
                                                <option value="worker1" className="opt-worker1"></option>
                                                <option value="worker2" className="opt-worker2"></option>
                                                <option value="worker3" className="opt-worker3"></option>
                                                <option value="worker4" className="opt-worker4"></option>
                                                <option value="worker5" className="opt-worker5"></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <h2>Active</h2>
                                        <h4 className="margb10" id="activeworker1"></h4>
                                        <h4 className="margb10" id="activeworker2"></h4>
                                        <h4 className="margb10" id="activeworker3"></h4>
                                        <h4 className="margb10" id="activeworker4"></h4>
                                        <h4 className="margb10" id="activeworker5"></h4>
                                        <h4 className="margb10" id="activeworker6"></h4>
                                        <h4 className="margb10" id="activeworker7"></h4>
                                        <h4 className="margb10" id="activeworker8"></h4>
                                        <h4 className="margb10" id="activeworker9"></h4>
                                        <h4 className="margb10" id="activeworker10"></h4>
                                    </div>
                                    <div className="col">
                                        <h2>%</h2>
                                        <h4 className="margb10" id="percent1"></h4>
                                        <h4 className="margb10" id="percent2"></h4>
                                        <h4 className="margb10" id="percent3"></h4>
                                        <h4 className="margb10" id="percent4"></h4>
                                        <h4 className="margb10" id="percent5"></h4>
                                        <h4 className="margb10" id="percent6"></h4>
                                        <h4 className="margb10" id="percent7"></h4>
                                        <h4 className="margb10" id="percent8"></h4>
                                        <h4 className="margb10" id="percent9"></h4>
                                        <h4 className="margb10" id="percent10"></h4>
                                    </div>
                                </div>
                            </div>
                            <div className="col available-worker">
                                <h3 className="text-center">Available Worker</h3>
                                <div className="row text-center">
                                    <div className="col">
                                        <h5>Name</h5>
                                        <h4 id="workerName1"></h4>
                                        <h4 id="workerName2"></h4>
                                        <h4 id="workerName3"></h4>
                                        <h4 id="workerName4"></h4>
                                        <h4 id="workerName5"></h4>
                                    </div>
                                    <div className="col">
                                        <h5>Ranking</h5>
                                        <h4 id="workerRanking1"></h4>
                                        <h4 id="workerRanking2"></h4>
                                        <h4 id="workerRanking3"></h4>
                                        <h4 id="workerRanking4"></h4>
                                        <h4 id="workerRanking5"></h4>
                                    </div>

                                </div>
                            </div>
                        </div>



                    </div>

                    <footer>

                    </footer>

                    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
                        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
                        crossorigin="anonymous"></script>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
                        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
                        crossorigin="anonymous"></script>
                    <script src="./scripts/script.js"></script>
                </div>
            </div>

        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(WarehouseGame)