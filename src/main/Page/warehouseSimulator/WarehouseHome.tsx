import React from "react";
import { Link } from "react-router-dom";
import { Button } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { connect } from "react-redux";
import { getTheme } from "../../theme/selectors/themeSelectors";
import { SupportedThemes } from "../../components/ThemeSelect";
import { getThemeValues } from "../../components/ThemeColors";
import { useTranslation } from "react-i18next";
import "../warehouseSimulator/styles/css/style.css"

interface Props {
    theme: SupportedThemes,
}

const WarehouseHome = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation("ProjectsNav");

    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg }}>
            <div className="container p-4 min-vh">
                <nav className="d-flex flex-wrap">
                    <Link to="/projects" className="mr-3 btn-link" style={{ color: themeValues.colors.btn }}>
                        <ArrowBackIcon />
                        <div className="ml-1">{t('back')}</div>
                    </Link>
                </nav>

                <div className="mt-4">
                    <div className="">
                        <h2>Warehouse Management Simulator</h2>
                        <p>En développement...</p>
                    </div>
                </div>
                <div className="menu-html">
                    <div className="menu-body gradient-bg">

                        <h1 className="h1">Warehouse Management Simulator</h1>

                        <div className="menu-main">
                            <a href="/projects/warehouse/howtoplay" className="menu-button text-center"><span>How to play</span></a>
                            <a className="menu-button text-center">Credits</a>
                            <a href="/projects/warehouse/game" className="menu-button text-center">Start</a>
                        </div>
                        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
                            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
                            crossorigin="anonymous"></script>
                        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
                            integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
                            crossorigin="anonymous"></script>
                        <script src="./scripts/script.js"></script>
                    </div>
                </div>
            </div>

        </main>
    )
}
const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(WarehouseHome)