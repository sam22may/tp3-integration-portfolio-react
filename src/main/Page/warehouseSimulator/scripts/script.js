$(function() {
    
$('#startBtn').on('click', getAllLines)

// variables globales
let totalLinesInput = 2000
let totalMagsInput = 10

// lines by mag
let linesMag1 
let linesMag2 
let linesMag3 
let linesMag4 
let linesMag5 
let linesMag6 
let linesMag7 
let linesMag8 
let linesMag9 
let linesMag10
let totalLines

// get document by Id
let IdLinesMag1 = $('#linesmag1')
let IdLinesMag2 = $('#linesmag2')
let IdLinesMag3 = $('#linesmag3')
let IdLinesMag4 = $('#linesmag4')
let IdLinesMag5 = $('#linesmag5')
let IdLinesMag6 = $('#linesmag6')
let IdLinesMag7 = $('#linesmag7')
let IdLinesMag8 = $('#linesmag8')
let IdLinesMag9 = $('#linesmag9')
let IdLinesMag10 = $('#linesmag10')
let IdTotalLines = $('#totallines')

// active worker
let activeWorker1 = $('#activeworker1')
let activeWorker2 = $('#activeworker2')
let activeWorker3 = $('#activeworker3')
let activeWorker4 = $('#activeworker4')
let activeWorker5 = $('#activeworker5')
let activeWorker6 = $('#activeworker6')
let activeWorker7 = $('#activeworker7')
let activeWorker8 = $('#activeworker8')
let activeWorker9 = $('#activeworker9')
let activeWorker10 = $('#activeworker10')

// percentage
let percent1 = $('#percent1')
let percent2 = $('#percent2')
let percent3 = $('#percent3')
let percent4 = $('#percent4')
let percent5 = $('#percent5')
let percent6 = $('#percent6')
let percent7 = $('#percent7')
let percent8 = $('#percent8')
let percent9 = $('#percent9')
let percent10 = $('#percent10')

// select option class worker
let optworker1 = $('.opt-worker1')
let optworker2 = $('.opt-worker2')
let optworker3 = $('.opt-worker3')
let optworker4 = $('.opt-worker4')
let optworker5 = $('.opt-worker5')

//worker
let worker1 = {name:"Johnny", ranking: ranking(20, 100), active: false}
let worker2 = {name:"Bobby", ranking: ranking(20, 100), active: false}
let worker3 = {name:"Cody", ranking: ranking(90, 100), active: false}
let worker4 = {name:"Samy", ranking: ranking(90, 100), active: false}
let worker5 = {name:"Tery", ranking: ranking(20, 100), active: false}

function ranking(min, max){
    let rndranking = Math.floor(Math.random() * (max - min + 1) + min)
    return rndranking
}

// options name
optworker1.text(worker1.name)
optworker2.text(worker2.name)
optworker3.text(worker3.name)
optworker4.text(worker4.name)
optworker5.text(worker5.name)

//available worker
////name
$('#workerName1').text(worker1.name)
$('#workerName2').text(worker2.name)
$('#workerName3').text(worker3.name)
$('#workerName4').text(worker4.name)
$('#workerName5').text(worker5.name)
////Ranking
$('#workerRanking1').text(worker1.ranking)
$('#workerRanking2').text(worker2.ranking)
$('#workerRanking3').text(worker3.ranking)
$('#workerRanking4').text(worker4.ranking)
$('#workerRanking5').text(worker5.ranking)

function getAllLines(){
    // remove disabled
    let arraySelects = document.getElementsByClassName('select');
    for(let i = 0; i < arraySelects.length; i++){
        arraySelects[i].removeAttribute('disabled')
    }

    function linesByMag(totalLinesInput){
        let linesByMag = totalLinesInput / totalMagsInput
        let randomlines = Math.floor(Math.random() * 200) + linesByMag
        return randomlines
    }
    
    linesMag1 = linesByMag(totalLinesInput)
    linesMag2 = linesByMag(totalLinesInput)
    linesMag3 = linesByMag(totalLinesInput)
    linesMag4 = linesByMag(totalLinesInput)
    linesMag5 = linesByMag(totalLinesInput)
    linesMag6 = linesByMag(totalLinesInput)
    linesMag7 = linesByMag(totalLinesInput)
    linesMag8 = linesByMag(totalLinesInput)
    linesMag9 = linesByMag(totalLinesInput)
    linesMag10 = linesByMag(totalLinesInput)
    totalLines = linesMag1 + linesMag2 + linesMag3 + linesMag4 + linesMag5 + linesMag6 + linesMag7 + linesMag8 + linesMag9 + linesMag10
    
    IdLinesMag1.text(linesMag1)
    IdLinesMag2.text(linesMag2)
    IdLinesMag3.text(linesMag3)
    IdLinesMag4.text(linesMag4)
    IdLinesMag5.text(linesMag5)
    IdLinesMag6.text(linesMag6)
    IdLinesMag7.text(linesMag7)
    IdLinesMag8.text(linesMag8)
    IdLinesMag9.text(linesMag9)
    IdLinesMag10.text(linesMag10)
    IdTotalLines.text(totalLines)
}


// mag 01 association
$('#worker-select-1').on('click', applyWorker)
$('#worker-select-2').on('click', applyWorker)
$('#worker-select-3').on('click', applyWorker)
$('#worker-select-4').on('click', applyWorker)
$('#worker-select-5').on('click', applyWorker)
$('#worker-select-6').on('click', applyWorker)
$('#worker-select-7').on('click', applyWorker)
$('#worker-select-8').on('click', applyWorker)
$('#worker-select-9').on('click', applyWorker)
$('#worker-select-10').on('click', applyWorker)

function applyWorker(e){
    console.log(e)
    let id = e.target.id
    console.log(id)
    let workerselect = $(`#${id}`)
    console.log(workerselect)
    let workerPick = workerselect.val()
    if(id === "worker-select-1"){
        selectWorkerPick(activeWorker1, linesMag1, IdLinesMag1, percent1)
    }
    if(id === "worker-select-2"){
        selectWorkerPick(activeWorker2, linesMag2, IdLinesMag2, percent2)
    }
    if(id === "worker-select-3"){
        selectWorkerPick(activeWorker3, linesMag3, IdLinesMag3, percent3)
    }
    if(id === "worker-select-4"){
        selectWorkerPick(activeWorker4, linesMag4, IdLinesMag4, percent4)
    }
    if(id === "worker-select-5"){
        selectWorkerPick(activeWorker5, linesMag5, IdLinesMag5, percent5)
    }
    if(id === "worker-select-6"){
        selectWorkerPick(activeWorker6, linesMag6, IdLinesMag6, percent6)
    }
    if(id === "worker-select-7"){
        selectWorkerPick(activeWorker7, linesMag7, IdLinesMag7, percent7)
    }
    if(id === "worker-select-8"){
        selectWorkerPick(activeWorker8, linesMag8, IdLinesMag8, percent8)
    }
    if(id === "worker-select-9"){
        selectWorkerPick(activeWorker9, linesMag9, IdLinesMag9, percent9)
    }
    if(id === "worker-select-10"){
        selectWorkerPick(activeWorker10, linesMag10, IdLinesMag10, percent10)
    }
    // function workerSelect(workerSelect){
    //     if(id === workerSelect){
    //         if(workerselect)
    //         selectWorkerPick(activeWorker, linesMag10, IdLinesMag10, percent10)
    //     }
    // }
    function selectWorkerPick(activeWorker, linesMag, IdLinesMag, percentShow){
        if(workerPick != ""){
            workerselect.prop('disabled', true)
            if(workerPick === "worker1"){
                optworker1.prop('disabled', true)
                activeWorker.text(worker1.name)
                doTheLines(linesMag, worker1.ranking, IdLinesMag, percentShow, workerselect, optworker1)
            }
            if(workerPick === "worker2"){
                optworker2.prop('disabled', true)
                activeWorker.text(worker2.name)
                doTheLines(linesMag, worker2.ranking, IdLinesMag, percentShow, workerselect, optworker2)
            }
            if(workerPick === "worker3"){
                optworker3.prop('disabled', true)
                activeWorker.text(worker3.name)
                doTheLines(linesMag, worker3.ranking, IdLinesMag, percentShow, workerselect, optworker3)
            }
            if(workerPick === "worker4"){
                optworker4.prop('disabled', true)
                activeWorker.text(worker4.name)
                doTheLines(linesMag, worker4.ranking, IdLinesMag, percentShow, workerselect, optworker4)
            }
            if(workerPick === "worker5"){
                optworker5.prop('disabled', true)
                activeWorker.text(worker5.name)
                doTheLines(linesMag, worker5.ranking, IdLinesMag, percentShow, workerselect, optworker5)
            }
        }
    }
}


function doTheLines(linesMag, workerRank, magId, percentShow, workerselect, optworker){
    let linesTiming = (linesMag/workerRank) * 60
    console.log(linesTiming)
    let timer = setInterval(linesDown, linesTiming)
    function linesDown(){
        totalLines--
        IdTotalLines.text(totalLines)
        linesMag--
        // percent
        magId.text(linesMag)
        if(linesMag <= 0){
            magId.text('0')
            clearInterval(timer)
        }
    }
    let percentTiming = linesTiming*(linesMag/100)
    let percent = 0
    let timerPercent = setInterval(percentDown, percentTiming)
    function percentDown(){
        percent++
        percentShow.text(percent+'%')
        if(percent >= 100){
            percentShow.text('100%')
            clearInterval(timerPercent)
            workerselect.val("")
            optworker.prop('disabled', false)
        }
    }
}

// function toggleDisability(selectElement) {
//     let arraySelects = document.getElementsByClassName('select');
//     let selectedOption = selectElement.selectedIndex;
//     for (let i = 0; i < arraySelects.length; i++) {
//         if (arraySelects[i] == selectElement)
//             continue; 
//         arraySelects[i].options[selectedOption].disabled = true;
//     }
//   }
  
})
//end of document ready function
