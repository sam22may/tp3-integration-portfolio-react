import React from "react";
import sam from "../../images/presentation/Sam1.jpg";
import studio from "../../images/presentation/home_studio.jpg";
import moto from "../../images/presentation/motocross.jpg";
import wakeboard from "../../images/presentation/wakeboard.jpg";
import { getThemeValues } from "../components/ThemeColors";
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
}

const PagePresentation = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation("Presentation")
    return (
        <main style={{ color: themeValues.colors.text1, backgroundColor: themeValues.colors.mainBg}}>
            <div className="py-5 text-justify container-xl">
                
                <div className="row mb-5">

                    <div className="col-lg-6 col-md-12">
                        <img src={sam} alt="Sam" className="mw-100 img-border" />
                    </div>
                    <div className="col-lg-6 col-md-12">
                        <h4 style={{ color: themeValues.colors.title }}>{t("journey")}</h4>
                        <p className="mb-3">{t("p1")}</p>
                        <p className="mb-3">{t("p2")}</p>
                        <p className="mb-3">{t("p3")}</p>
                    </div>
                </div>

                <div className="row mb-5">
                    <div className="col-lg-6 col-md-12">
                        <h4 style={{ color: themeValues.colors.title }}>Passions</h4>
                        <p className="mb-3">{t("p4")}</p>
                    </div>
                    <div className="col-lg-6 col-md-12">
                        <img src={studio} alt="studio" className="mw-100 img-border" />
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-3 col-md-6">
                        <img src={wakeboard} alt="wakeboard" className="mw-100 img-border" />
                    </div>
                    <div className="col-lg-3 col-md-6">
                        <img src={moto} alt="moto" className="mw-100 img-border" />
                    </div>
                    <div className="col-lg-6 col-md-12">
                        <p className="mb-3">{t("p5")}</p>
                        <p className="mb-3">{t("p6")}</p>
                    </div>
                </div>
            </div>
        </main>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(PagePresentation)