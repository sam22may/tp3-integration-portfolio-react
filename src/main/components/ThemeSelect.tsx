import { Box, FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";
import React, { useState } from "react";
import { toggleTheme } from "../theme/action/themeAction";
import { bindActionCreators } from "redux"
import { connect } from 'react-redux'
import { getThemeProps } from '@mui/system';
import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import { useTranslation } from "react-i18next";

export enum SupportedThemes {
    LIGHT = "Claire",
    DARK = "Sombre",
}

interface Props {
    theme: any,
    actions: {
        toggleTheme: (theme: SupportedThemes) => void
    }
}

const ThemeSelect = (props: Props) => {
    const [theme, setTheme] = useState<SupportedThemes>(SupportedThemes.LIGHT)

    const handleChange = (event: any) => {
        const newTheme = event.target.value

        setTheme(newTheme)
        props.actions.toggleTheme(newTheme)
    }

    const { t } = useTranslation("Nav");

    return (
        <Box sx={{ minWidth: 40 }}>
            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label" className="font-weight-bold text-white">{t("theme")}</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={theme}
                    label="Thème"
                    onChange={handleChange}
                    className="text-center select"
                >
                    <MenuItem value={SupportedThemes.LIGHT}><LightModeIcon /></MenuItem>
                    <MenuItem value={SupportedThemes.DARK}><DarkModeIcon /></MenuItem>
                </Select>
            </FormControl>
        </Box>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getThemeProps(state)
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        actions: bindActionCreators({
            toggleTheme: toggleTheme,
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThemeSelect);