import i18n from "../i18n";
import { useState } from "react";

interface Props {
    language: any,
}

const LanguageSelect = (props: Props) => {
    const btnBg = "#0099ff";
    const btnNoBg = "transparent";
    const [btnFrColor, setBtnFrColor] = useState(btnNoBg);
    const [btnEnColor, setBtnEnColor] = useState(btnNoBg);

    const changeBtnColor = (lng: any) => {
        if (lng === "fr") {
            setBtnFrColor(btnBg)
            setBtnEnColor(btnNoBg)
        } else {
            setBtnFrColor(btnNoBg)
            setBtnEnColor(btnBg)
        }
    }

    const changeLanguage = (lng: any) => {
        changeBtnColor(lng)
        i18n.changeLanguage(lng)
    }

    return (
        <div>
            <button className="btn-lng" onClick={() => changeLanguage('fr')} style={{ backgroundColor: btnFrColor }}>Fr</button>
            <button className="btn-lng" onClick={() => changeLanguage('en')} style={{ backgroundColor: btnEnColor }}>En</button>
        </div>
    )
}

export default LanguageSelect;