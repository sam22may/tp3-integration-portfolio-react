import { SupportedThemes } from  "./ThemeSelect";

interface Theme {
    colors: {
        headerBg: string,
        navBg: string,
        mainBg: string,
        cardBg: string,
        text1: string,
        text2: string,
        title: string,
        btn: string,
    }
}

const BlueLight = "#deeffd"
const BlueLight2 = '#99d1ff'
const BlueLight3 = '#e8f5ff'
const Blue = "#48adff"
const Blue2 = "#0095f8"
const White = "#fff"
const BlueDark = "#002442"
const BlueDark2 = "#004b88"
const BlueDark3 = "#011525"
const BlueBtn = "#1976d2"

const colorPlaceholder = "#e40606"

const darkTheme: Theme = {
    colors: {
        headerBg: BlueDark,
        navBg: BlueDark2,
        mainBg: BlueDark3,
        cardBg: BlueDark2,
        text1: BlueLight,
        text2: colorPlaceholder,
        title: Blue2,
        btn: BlueLight,
    }
}

const lightTheme: Theme = {
    colors: {
        headerBg: BlueLight2,
        navBg: Blue,
        mainBg: BlueLight3,
        cardBg: White,
        text1: BlueDark,
        text2: colorPlaceholder,
        title: BlueDark2,
        btn: BlueBtn,
    }
}

export const getThemeValues = (supportedThemes: SupportedThemes) => {
    if (supportedThemes === SupportedThemes.DARK) {
        return darkTheme;
    } else {
        return lightTheme;
    }
}
