import React from "react";
import { Link } from "react-router-dom";
import '../../App.css';
import { ReactComponent as Logo } from "../../images/logo-s.svg"
import { getThemeValues } from "../components/ThemeColors"
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";

interface Props {
    theme: SupportedThemes
}

const Footer = (props: Props) => {
    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation('Footer');
    return (
        <footer style={{ backgroundColor: themeValues.colors.headerBg }} className="footer bg-shadow">
            <div style={{ color: themeValues.colors.title }} className="d-flex align-items-center justify-content-center">
                <Link to="/cv">
                    <p>{t('resume')}</p>
                </Link>
                <Link to="/">
                    <Logo style={{ fill: themeValues.colors.title }} width="100px" className="m-3" />
                </Link>
                <Link to="/contact">
                    <p>Contact</p>
                </Link>
            </div>
            <p style={{ color: themeValues.colors.text1 }} className="fz-16 text-center">{t("rights")} &#62; Samuel Le May &copy; 2021</p>
        </footer>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(Footer)