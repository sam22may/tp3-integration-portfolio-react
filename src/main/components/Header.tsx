import React from "react";
import { connect } from "react-redux";
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "./ThemeSelect";
import '../../App.css';
import { ReactComponent as Logo} from "../../images/logo-s.svg";
import { Link } from "react-router-dom";
import { getThemeValues } from "./ThemeColors";
import { useTranslation } from "react-i18next";


interface Props {
    theme: SupportedThemes,
    valeur: string,
}

const Header = (props: Props) => {

    const themeValues = getThemeValues(props.theme);
    const { t } = useTranslation("Header");

    return (
        <header className="header border-bot d-flex align-items-center justify-content-between" style={{ backgroundColor: themeValues.colors.headerBg }}>
            <h1 className="p-3" style={{ color: themeValues.colors.title }}>Samuel Le May &gt; {t("dev")} {props.valeur}</h1> 
            <Link to="/">
                <Logo width="100px" className="m-3 logo" style={{ fill: themeValues.colors.title }} />
            </Link>
        </header>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => {})(Header);