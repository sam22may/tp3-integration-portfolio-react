import React from "react";
import { Route, Switch } from "react-router";
import PageAccueil from "../Page/PageAccueil";
import PagePresentation from "../Page/PagePresentation";
import PageContact from "../Page/PageContact";
import PageProjets from "../Page/PageProjets";
import PageCV from "../Page/PageCV";
import PageSkills from "../Page/PageSkills";
import ProjetListe from "../Page/Projets/ProjetListe";
import ProjetListeDocumentation from "../Page/Projets/ProjetListeDocumentation";
import ProjetPremierPortfolio from "../Page/Projets/ProjetPremierPortfolio";
import ProjetAffichesDj from "../Page/Projets/ProjetAffichesDj";
import ProjetImmologi from "../Page/Projets/ProjetImmologi";
import ProjetTrnxShop from "../Page/Projets/ProjetTrnxShop";
import ProjetCatApi from "../Page/Projets/ProjetCatApi";
import ProjetCatApiDevis from "../Page/Projets/ProjetCatApiDevis";
import ProjetQuiz from "../Page/Projets/ProjetQuiz";
import ProjetQuizDevis from "../Page/Projets/ProjetQuizDevis";
import ProjetQuizCorrection from "../Page/Projets/ProjetQuizCorrection";
import ProjetMotocross from "../Page/Projets/ProjetMotocross";
import ProjetMotocrossDevis from "../Page/Projets/ProjetMotocrossDevis";
import ProjetFaitMonMenage from "../Page/Projets/ProjetFaitMonMenage";
import ProjetFaitMonMenageDevis from "../Page/Projets/ProjetFaitMonMenageDevis";
import ProjetImmologiDevis from "../Page/Projets/ProjetImmologiDevis";
import Header from "../components/Header";
import ProjetPremierPortfolioDevis from "../Page/Projets/ProjetPremierPortfolioDevis";
import ProjetListeDevis from "../Page/Projets/ProjetListeDevis";
import ProjetNourriture from "../Page/Projets/ProjetNourriture";
import ProjetNourritureDevis from "../Page/Projets/ProjetNourritureDevis";
import WarehouseHome from "../Page/warehouseSimulator/WarehouseHome";
import WarehouseGame from "../Page/warehouseSimulator/WarehouseGame";
import { useTranslation } from "react-i18next";


const AppNavigator = () => {
    const { t } = useTranslation('Header');
    return (
        <Switch>
            <Route exact path="/">
                <Header valeur="" />
                <PageAccueil />
            </Route>

            <Route exact path="/presentation">
                <Header valeur={"> " + t("presentation")} />
                <PagePresentation />
            </Route>

            <Route exact path="/skills">
                <Header valeur={"> " + t("skills")} />
                <PageSkills />
            </Route>

            <Route exact path="/projects">
                <Header valeur={"> " + t("projects")} />
                <PageProjets />
            </Route>

            <Route exact path="/projects/liste">
                <Header valeur={"> " + t("projects")} />
                <ProjetListe />
            </Route>

            <Route exact path="/projects/liste/documentation">
                <Header valeur={"> " + t("projects")} />
                <ProjetListeDocumentation />
            </Route>

            <Route exact path="/projects/liste/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetListeDevis />
            </Route>

            <Route exact path="/projects/premierportfolio">
                <Header valeur={"> " + t("projects")} />
                <ProjetPremierPortfolio />
            </Route>

            <Route exact path="/projects/premierportfolio/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetPremierPortfolioDevis />
            </Route>

            <Route exact path="/projects/affichesdj">
                <Header valeur={"> " + t("projects")} />
                <ProjetAffichesDj />
            </Route>

            <Route exact path="/projects/trnxshop">
                <Header valeur={"> " + t("projects")} />
                <ProjetTrnxShop />
            </Route>

            <Route exact path="/projects/agenceimmologi">
                <Header valeur={"> " + t("projects")} />
                <ProjetImmologi />
            </Route>

            <Route exact path="/projects/agenceimmologi/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetImmologiDevis />
            </Route>

            <Route exact path="/projects/catapi">
                <Header valeur={"> " + t("projects")} />
                <ProjetCatApi />
            </Route>

            <Route exact path="/projects/catapi/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetCatApiDevis />
            </Route>

            <Route exact path="/projects/quiz">
                <Header valeur={"> " + t("projects")} />
                <ProjetQuiz />
            </Route>

            <Route exact path="/projects/quiz/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetQuizDevis />
            </Route>

            <Route exact path="/projects/quiz/correction">
                <Header valeur={"> " + t("projects")} />
                <ProjetQuizCorrection />
            </Route>

            <Route exact path="/projects/motocross">
                <Header valeur={"> " + t("projects")} />
                <ProjetMotocross />
            </Route>

            <Route exact path="/projects/motocross/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetMotocrossDevis />
            </Route>

            <Route exact path="/projects/faitmonmenage">
                <Header valeur={"> " + t("projects")} />
                <ProjetFaitMonMenage />
            </Route>

            <Route exact path="/projects/faitmonmenage/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetFaitMonMenageDevis />
            </Route>

            <Route exact path="/projects/nourriture">
                <Header valeur={"> " + t("projects")} />
                <ProjetNourriture />
            </Route>

            <Route exact path="/projects/nourriture/devis">
                <Header valeur={"> " + t("projects")} />
                <ProjetNourritureDevis />
            </Route>

            <Route exact path="/projects/warehouse">
                <Header valeur={"> " + t("projects")} />
                <WarehouseHome />
            </Route>

            <Route exact path="/projects/warehouse/howtoplay">
                <Header valeur={"> " + t("projects")} />
                <WarehouseHome />
            </Route>

            <Route exact path="/projects/warehouse/game">
                <Header valeur={"> " + t("projects")} />
                <WarehouseGame />
            </Route>

            <Route exact path="/contact">
                <Header valeur={"> Contact"} />
                <PageContact />
            </Route>

            <Route exact path="/cv">
                <Header valeur={"> " + t("resume")} />
                <PageCV />
            </Route>

        </Switch>
    )
}

export default AppNavigator;