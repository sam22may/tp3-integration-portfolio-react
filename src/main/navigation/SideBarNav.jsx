import React from "react";
import { connect } from "react-redux";
import { getTheme } from "../theme/selectors/themeSelectors";
import { SupportedThemes } from "../components/ThemeSelect";
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import ThemeSelect from "../components/ThemeSelect";
import { Link } from "react-router-dom";
import HomeIcon from '@mui/icons-material/Home';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import SourceIcon from '@mui/icons-material/Source';
import InfoIcon from '@mui/icons-material/Info';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import { useTranslation } from "react-i18next";
import LanguageSelect from "../components/LanguageSelect";
import { useHistory, useLocation } from "react-router-dom";


interface Props {
    theme: SupportedThemes
}

const SideBarNav = (props: Props) => {
    const colorBlueLight = "#deeffd"
    const colorBlue = "#48adff"
    const colorBlueDark = "#002442"
    const colorBlueDark2 = "#004b88"

    const { t } = useTranslation("Nav");

    const getBgColor2 = () => {
        if (props.theme === SupportedThemes.LIGHT) {
            return colorBlue
        }
        else {
            return colorBlueDark2
        }
    }

    const getTextColor = () => {
        if (props.theme === SupportedThemes.LIGHT) {
            return colorBlueDark
        }
        else {
            return colorBlueLight
        }
    }

    let history = useHistory();
    let location = useLocation();

    return (
        <SideNav
            className="sidenav-custom inner-shadow"
            style={{ backgroundColor: getBgColor2() }}
            onSelect={(selected) => {
                if (selected !== undefined) {
                    const to = '/' + selected;
                    if (location.pathname !== to) {
                        history.push(to);
                    }
                }
            }}
        >
            <SideNav.Toggle />
            <SideNav.Nav defaultSelected="home">
                <NavItem eventKey="home">
                    <NavIcon>
                        <Link to="/">
                            <HomeIcon style={{ color: getTextColor() }} />
                        </Link>
                    </NavIcon>
                    <NavText>
                        <Link to="/" style={{ color: getTextColor() }}>{t("home")}</Link>
                    </NavText>
                </NavItem>

                <NavItem eventKey="presentation">
                    <NavIcon>
                        <Link to="/presentation">
                            <AccessibilityIcon style={{ color: getTextColor() }} />
                        </Link>
                    </NavIcon>
                    <NavText>
                        <Link to="/presentation" style={{ color: getTextColor() }}>{t("presentation")}</Link>
                    </NavText>
                </NavItem>

                <NavItem eventKey="skills">
                    <NavIcon>
                        <Link to="/skills">
                            <AssignmentTurnedInIcon style={{ color: getTextColor() }} />
                        </Link>
                    </NavIcon>
                    <NavText>
                        <Link to="/skills" style={{ color: getTextColor() }}>{t("skills")}</Link>
                    </NavText>
                </NavItem>

                <NavItem eventKey="projects">
                    <NavIcon>
                        <Link to="/projects">
                            <SourceIcon style={{ color: getTextColor() }} />
                        </Link>
                    </NavIcon>
                    <NavText>
                        <Link to="/projects" style={{ color: getTextColor() }}>{t("projects")}</Link>
                    </NavText>
                </NavItem>

                <NavItem eventKey="contact">
                    <NavIcon>
                        <Link to="/contact">
                            <InfoIcon style={{ color: getTextColor() }} />
                        </Link>
                    </NavIcon>
                    <NavText>
                        <Link to="/contact" style={{ color: getTextColor() }}>Contact</Link>
                    </NavText>
                </NavItem>

                <NavItem eventKey="cv">
                    <NavIcon>
                        <Link to="/cv">
                            <ContactPageIcon style={{ color: getTextColor() }} />
                        </Link>
                    </NavIcon>
                    <NavText>
                        <Link to="/cv" style={{ color: getTextColor() }}>{t("resume")}</Link>
                    </NavText>
                </NavItem>

                <NavItem className="mt-1">
                    <ThemeSelect />
                </NavItem>

                <NavItem className="mt-1">
                    <LanguageSelect />
                </NavItem>

            </SideNav.Nav>
        </SideNav>
    )
}

const mapStateToProps = (state: any) => {
    return {
        theme: getTheme(state),
    }
}

export default connect(mapStateToProps, () => { })(SideBarNav);