import merge from "deepmerge";
import Home from "../Page/i18n/Translations";
import Nav from "../Page/i18n/Translations";
import Header from "../Page/i18n/Translations"

export default merge.all([Home, Nav, Header]);