import React from 'react';
import './App.css';
import configureStore from './main/reducers/configureStore';
import { Provider } from 'react-redux'
import AppNavigator from './main/navigation/AppNavigator';
import { BrowserRouter as Router } from "react-router-dom";
import SideBarNav from './main/navigation/SideBarNav';
import Footer from './main/components/Footer'
import { I18nextProvider } from 'react-i18next';
import i18n from "./main/i18n";
import ScrollToTop from './main/components/ScrollToTop';


const store = configureStore()

const App = () => {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <div className="App">
          <Router>
            <ScrollToTop />
            <div className="d-flex">
              <SideBarNav />
              <div className="w-100">
                <AppNavigator />
                <Footer />
              </div>
            </div>
          </Router>
        </div>
      </Provider>
    </I18nextProvider>
  );
}

export default App;
